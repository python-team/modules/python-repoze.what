python-repoze.what (1.0.9-6) UNRELEASED; urgency=medium

  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 22:21:17 +0200

python-repoze.what (1.0.9-5) unstable; urgency=medium

  * Using cannonical VCS URLs.
  * Ran wrap-and-sort.
  * Switch away from CDBS, and now uses dh short style.
  * Adds extend-diff-ignore = "^[^/]*[.]egg-info/" in debian/source/options.
  * Using dh_sphinxdoc correctly. Not packaging README.txt which is the exact
    copy of the long description.
  * Switching to source format 3.0 (quilt).
  * Patches docs/source/conf.py to stop using intersphinx.
  * rm -f /usr/lib/python2*/dist-packages/repoze.what-*-nspkg.pth
    (Closes: #746023).
  * Package build-conflicts with itself to avoid FTBFS with unit tests.
  * Rewrite debian/copyright into parseable format 1.0.
  * Standards-Version: is now 3.9.5.
  * Set Maintainer: Debian Python Modules Team, with Thomas Goirand & Dimitri
    John Ledkov as uploader.

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 May 2014 23:09:11 +0000

python-repoze.what (1.0.9-4) unstable; urgency=medium

  * QA upload.
  * Bump debhelper to 9.
  * Switch to dh_python2.

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Sun, 20 Apr 2014 14:05:01 +0100

python-repoze.what (1.0.9-3) unstable; urgency=medium

  * QA upload.
  * Move call to dh_link in debian/rules to a proper debian/links file,
    so that debhelper just does the right thing (Closes: #738384)

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 23 Mar 2014 16:56:38 +0000

python-repoze.what (1.0.9-2) unstable; urgency=low

  * Orphan package.

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 03 May 2012 10:44:58 +0200

python-repoze.what (1.0.9-1) unstable; urgency=low

  * New upstream release
  * Add python-repoze.who (<= 1.99) to Depends
  * Add libjs-jquery to Suggests and use its jquery.js instead of embedded one
  * bump Standards-Version to 3.8.4 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 19 Apr 2010 23:15:43 +0200

python-repoze.what (1.0.8-3) unstable; urgency=low

  * first upload to unstable
  * debian/control: add Piotr to Uploaders

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 02 Aug 2009 13:45:54 +0200

python-repoze.what (1.0.8-2) experimental; urgency=low

  [ Piotr Ożarowski ]
  * Add python-zopeinterface to Depends
  * Add python-pkg-resources to Recommends
  * Bump minimum required versions:
    - python-paste to 1.7
    - python-repoze.who to 1.0

  [ Stefano Zacchiroli ]
  * use "python-zope.interface" instead of "python-zopeinterface" (new
    package name, by the Zope team)
  * bump Standards-Version to 3.8.2 (no changes needed)

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 05 Jul 2009 16:10:33 +0200

python-repoze.what (1.0.8-1) experimental; urgency=low

  * First release (Closes: #531045)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 May 2009 22:56:24 +0200
